const express = require('express')
const router = express.Router()
const utils = require('../utils')
const db = require('../db')
const { request, response } = require('express')

router.get('/name/:name',(request, response)=>{
    const {name} = request.params

    const query = `SELECT * FROM product WHERE name ='${name}'`

    db.execute(query,(error,result) => {
        if(result.length > 0){
            response.send(utils.createResult(error, result))
        }else{
            response.send('Product not found !!!')
        }
    })
})
router.post('/addproduct',(request, response)=>{
    const {name, category, price} = request.body

    const query = `INSERT INTO product (name, category, price) VALUES ('${name}','${category}','${price}')`
    db.execute(query,(error,result) => {
        response.send(utils.createResult(error, result))    
    })
})
router.put('/name/:name',(request, response)=>{
    const {name} = request.params
    const {price} = request.body

    const query = `UPDATE product SET price = '${price}' WHERE name='${name}'`
    db.execute(query,(error,result) => {
        response.send(utils.createResult(error, result))    
    })
})
router.delete('/delete/:name',(request, response)=>{
    const {name} = request.params

    const query = `DELETE FROM product WHERE name='${name}'`
    db.execute(query,(error,result) => {
        response.send(utils.createResult(error, result))    
    })
})

module.exports = router
