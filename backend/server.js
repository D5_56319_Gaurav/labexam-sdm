const express = require('express')
const app = express()
const cors = require('cors')
const router = require('./routes/product')

app.use(cors('*'))
app.use(express.json())
app.use('/product',router)

app.listen(4000, '0.0.0.0',()=>{
    console.log('Server started on port 4000')
})