CREATE TABLE IF NOT EXISTS product(
    id int PRIMARY KEY auto_increment,
    name VARCHAR(50),
    category VARCHAR(50),
    price double
);